# svelte-tailwindui-freebies

Tailwind UI [examples](https://tailwindui.com/preview) ported to Svelte, powered by [`create-svelte`](https://github.com/sveltejs/kit/tree/master/packages/create-svelte) and [`svelte-headlessui`](https://github.com/rgossiaux/svelte-headlessui);

## Demo

Visit [svelte-tailwindui-freebies.vercel.app](https://svelte-tailwindui-freebies.vercel.app) for interactive demo.

## Developing

Once you've cloned the project and installed dependencies with `pnpm i`, start a development server:

```bash
pnpm dev

# or start the server and open the app in a new browser tab
pnpm dev -- --open
```

## Building

Before creating a production version of your app, install an [adapter](https://kit.svelte.dev/docs#adapters) for your target environment. Then:

```bash
pnpm build
```

> You can preview the built app with `pnpm preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.

## Contributing

All pull/merge requests should be submitted in a single, squashed and [signed](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work) commit following the [Angular Commit Message Format](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-format). See the script `commit` inside the `package.json` package manifest for an example of how to create such a commit message from the command line.

### Legal

When you contribute you agree to make available the entire contents of your contribution under the current license used by the project at the time of submission. You further acknowledge that no other individual, business or government entity may claim copyright ownership over the work you are submitting.

Finally, you recognize the LICENSE of this project may change at any time with or without notice and if you do not agree with the terms of the new license change you MUST submit a written request to explicitly opt-out of such change within 30 days of the change taking place for your request to remain valid. For the sake of simplicity written requests may be submitted in the form of an Issue to the original source repository.

## Deploying

Deploys to Vercel with a single command:

```sh
vercel

# or deploy to production
vercel --prod
```

Requires a Vercel account. Does not require a GitHub repo.

## Rights

I am not your lawyer. With the exception of the stuff in the `static/designs` folder you just do whatever the fuck you want to. The design material is ostensibly copyright Tailwind Labs but I copied it here anyway because it's already on the Web and I didn't agree to any contracts expressly forbidding me from doing so.
